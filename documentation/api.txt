USERS
  add a user:
      /api/users/
      POST
      body:
        {
          "firstname": string,
          "lastname": string,
          "email": string,
          "password": string
        }
      response:
      {
        status: 200 or 400
        body:
          {
            "firstname": string,
            "lastname": string,
            "email": string
          }
      }

  remove a user
      /api/users?email={email}
      DELETE
      body: null
      response:
        {
          status: 204
        }

  update a user
      /api/users/
      PUT
      body:
        {
          "firstname": string, // can be null, will remain the same if so
          "lastname": string,  // can be null, will remain the same if so
          "email": string,
          "password": string   // can be null, will remain the same if so
        }
      response:
        status: 204

  get users
      /api/users/
      GET
      body: N/A
      response:
        body:
          [
            {
              "firstname": string,
              "lastname": string,
              "email": string,
            }
          ]


CONFERENCES      ###################################  !!!!!!!!  Conferences also have name attribute !!!!!!!!!!!!!!! ####################################################### 
  add Conferences
      /api/conferences/
      POST
      body:
        {
		  name: string,
          description: string,
          zeroDeadline: Date,
          abstractDeadline: Date | null,
          proposalDeadline: Date,
          biddingDeadline: Date,
          evaluationDeadline: Date,
          presentationDeadline: Date,
          participants: Email[] | null,
          scms: Email[] | null,
          cscm: Email | null,
          ccscm: Email | null,
          pcms: Email[] | null,
          cpcm: Email | null,
          ccpcm: Email | null,
        }
      response:
        status: 200 or 400
        body:
          {
            id: ID
			name: string,
            description: string,
            zeroDeadline: Date,
            abstractDeadline: Date | null,
            proposalDeadline: Date,
            biddingDeadline: Date,
            evaluationDeadline: Date,
            presentationDeadline: Date,
            participants: Email[] | null,
            scms: Email[] | null,
            cscm: Email | null,
            ccscm: Email | null,
            pcms: Email[] | null,
            cpcm: Email | null,
            ccpcm: Email | null,
            sections: Section[] | null
          }

  remove Conferences
      /api/conferences?id={id}
      DELETE
      body: null
      response: 204

  update Conference
      /api/conferences/
      PUT
      body:
        {
            id: ID,
			name: String, 
            description: string,
            zeroDeadline: Date,
            abstractDeadline: Date | null,
            proposalDeadline: Date,
            biddingDeadline: Date,
            evaluationDeadline: Date,
            presentationDeadline: Date,
            participants: Email[] | null,
            scms: Email[] | null,
            cscm: Email | null,
            ccscm: Email | null,
            pcms: Email[] | null,
            cpcm: Email | null,
            ccpcm: Email | null,
        }
      response:
        status: 200 or 400
        body:
          {
            id: ID,
			name: string
            description: string,
            zeroDeadline: Date,
            abstractDeadline: Date | null,
            proposalDeadline: Date,
            biddingDeadline: Date,
            evaluationDeadline: Date,
            presentationDeadline: Date,
            participants: Email[] | null,
            scms: Email[] | null,
            cscm: Email | null,
            ccscm: Email | null,
            pcms: Email[] | null,
            cpcm: Email | null,
            ccpcm: Email | null,
            sections: Section[] | null
          }

  get conferences
      /api/conferences/
      GET
      body: N/A
      response:
        body:
          [
            {
              id: ID
			  name: string,
              description: string,
              zeroDeadline: Date,
              abstractDeadline: Date | null,
              proposalDeadline: Date,
              biddingDeadline: Date,
              evaluationDeadline: Date,
              presentationDeadline: Date,
              participants: Email[] | null,
              scms: Email[] | null,
              cscm: Email | null,
              ccscm: Email | null,
              pcms: Email[] | null,
              cpcm: Email | null,
              ccpcm: Email | null,
              sections: Section[] | null
            }
          ]

  add participants on conference
      /api/conferences/{conferenceid}/participants/
      PUT
      body:
        {
          "email": String
        }
      response:
        status: 204

  remove participants on conference
      /api/conferences/{conferenceid}/participants?email={email}
      DELETE
      response:
        status: 204

  add section
      /api/conferences/{conferenceid}/sections
      POST
      body:
        {
          name: string,
          topics: string[],
          proposals: ID[] | null,
          participants: Email[] | null,
          chair: Email,
          room: number
        }
      response:
        status: 200 or 400
        body:
          {
            id: ID,
            name: string,
            topics: string[],
            proposals: ID[] | null,
            participants: Email[] | null,
            chair: Email,
            room: number
          }
    
  update section
      /api/conferences/{conferenceId}/sections
      PUT
      body:
        {
          id: ID,
          name: string,
          topics: string[],
          proposals: ID[] | null,
          participants: Email[] | null,
          chair: Email,
          room: number
        }

  remove section
      /api/conferences/{conferenceid}/sections?id={sectionid}
      DELETE
      body: N/A
      response:
        status: 204

  add participant to section
      /api/conferences/{conferenceid}/sections/{sectionid}/participants
      PUT
      body:
        {
          "email": String
        }
      response:
        status: 204

  remove participant to section
      /api/conferences/{conferenceid}/sections/{sectionid}/participants?email={email}
      DELETE
      response:
        status: 204


PROPOSALS
  add proposal
      /api/conferences/{conferenceid}/proposals
      POST
      body:
        {
          conference: ID | null,
          proposalName: string,
          file: string | null,  // a hexadecimal representation of the file; must be decoded and saved locally on the server
          abstract: string,     // abstract will be a plain old string
          topics: string[],
          keywords: string[] | null,
          author: Email,
          coAuthors: Email[] | null,
        }
      response:
        status: 200 or 400
        body:
         {
            id: ID,
            conference: ID | null,
            proposalName: string,
            filePath: string | null,
            abstract: string,
            topics: string[],
            keywords: string[] | null,
            author: Email,
            coAuthors: Email[] | null,
            bidders: Email[] | null,
            reviewers: Email[] | null,
            secondHandReviewer: Email[] | null
         }

  remove proposal
      /api/conferences/{conferenceid}/proposals?id={proposalId}
      DELETE
      body:
      response:
        status: 204

  get proposals
      /api/conferences/{conferenceid}/proposals
      GET
      body: N/A
      response:
        status: 200 or 400
        [
            id: ID,
            conference: ID | null,
            proposalName: string,
            filePath: string | null,
            abstract: string,
            topics: string[],
            keywords: string[] | null,
            author: string,
            coAuthors: Email[] | null,
            bidders: Email[] | null,
            reviewers: Email[] | null,
            secondHandReviewer: Email[] | null
        ]

  upload file to proposal        // also sets Proposal filePath to download link
      /api/conferences/{conferenceid}/proposals/{proposalid}/upload
      POST
      body: form-data
      response: 
        status: 200 or 400
        body:
        {
          "fileName": string,
          "fileDownloadUri": string,  // this is the download link
          "fileType": string,
          "size": int  //size in bytes
        }
        
  get file from proposal
      /api/conferences/{conferenceid}/proposals/{proposalid}/downloadFile/{fileId}
      GET
      body: N/A
      response: 
        status: 200 or 400
        response header: CONTENT_DISPOSITION + filename
        body: attachment bytes
      
  add review
      // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      // IMPORTANT -> proposal was renamed into proposalId
      // AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
      /api/conferences/{conferenceid}/proposals/{proposalid}/reviews
      POST
      body:
        {
          proposalId: ID,
          reviewer: Email,
          grade: number,
          justification: string
        }
      response:
        status: 200 or 400
        body:
        {
          id: ID,
          proposalId: ID,
          reviewer: Email,
          grade: number,
          justification: string
        }

  update review
    `${domain}/api/conferences/${conferenceId}/proposals/${proposalId}/reviews`
    PUT
    body:
      {
        proposalId: ID
        reviewer: Email,
        grade: number,
        justification: string
      }
    response:
      status: 204 or 400

  add reviewers
    `${domain}/api/conferences/${conferenceId}/proposals/${proposalId}/reviewers`
	PUT
    body:
	  Email[]
	response:
      status: 204 or 400
	  
  remove reviewers
    `${domain}/api/conferences/${conferenceId}/proposals/${proposalId}/reviewers`
	DELETE
    body:
	  Email[]
	response:
      status: 204 or 400
	  
  add second hand reviewer
    `${domain}/api/conferences/${conferenceId}/proposals/${proposalId}/sh`
    PUT
    body:
      {
        email: Email
      }
    response:
      status: 204 or 400 

  get reviews
      /api/conferences/{conferenceid}/proposals/{proposalid}/reviews
      GET
      body: N/A
      response:
        status: 200 or 400
        body:
          [
            {
              id: ID,
              proposalId: ID,
              reviewer: Email,
              grade: number,
              justification: string
            }
          ]

  bid
      /api/conferences/{conferenceid}/proposals/{proposalid}/bid
      PUT
      body:
        {
          "email": String
        }
      response:
        status: 204 or 400
  
  unbid
      /api/conferences/{conferenceid}/proposals/{proposalid}/unbid
      PUT
      body:
        {
          "email": String
        }
      response:
        status: 204 or 400


NOTIFICATIONS:
  get notifications
      /api/notifications
      GET
      response:
        status: 200 or 400
        body:
          {
            id: ID | null,
            text: string,
            href: string
            read: boolean
          }

  mark notification as read
      /api/notifications/{notificationId}
      PATCH
      response:
        status: 204


AUTHENTICATION
  login
      /api/login
      POST
      response:
        status: 200 or 400
        body:
          {
            token: JWT
          }

  register
      /api/register
      POST
      response:
        status: 200 or 400
        body:
          {
            firstname: response.firstname,
            lastname: response.lastname,
            email: response.email,
            token: response.token
          }